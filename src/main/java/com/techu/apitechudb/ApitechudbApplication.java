package com.techu.apitechudb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApitechudbApplication {

	public static void main(String[] args) {

		SpringApplication.run(ApitechudbApplication.class, args);
		System.out.println("ApitechudbApplication on");
	}

	//5-cent de la estructura de la aplicación
	//Modelo: Bean. Abstracción de la lógica de negocio
	//Repositorio: Punto más cercano a la BBDD
	//Consulta: la query será un objeto
	//Conexión: Pool. Singleton. Reutilizable. No la manejamos directamente.
	//Servicio: Capa que abstrae las operaciones con el exterior de la aplicación
	//Controlador: Conecta la vista con los datos
}
