package com.techu.apitechudb.controllers;

import com.techu.apitechudb.models.PurchaseModel;
import com.techu.apitechudb.services.PurchaseService;
import com.techu.apitechudb.services.PurchaseServiceResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")
public class PurchaseController {

    @Autowired
    PurchaseService purchaseService;

    @GetMapping("/purchases")
    public ResponseEntity<List<PurchaseModel>> getPurchases() {
        System.out.println("PurchaseController - getPurchases()");

        return new ResponseEntity<>(
                purchaseService.getPurchases(),
                HttpStatus.OK
        );
    }

    @PostMapping("/purchases")
    public ResponseEntity<PurchaseServiceResponse> addPurchase(@RequestBody PurchaseModel purchase) {
        System.out.println("PurchaseController - addPurchase()");
        System.out.println("ProductoController - la id del purchase a crear es : "+purchase.getId());
        System.out.println("ProductoController - la userId del purchase a crear es : "+purchase.getUserId());
        System.out.println("ProductoController - el amount del purchase a crear es : "+purchase.getAmount());
        System.out.println("ProductoController - el amount del purchaseItems a crear es : "+purchase.getPurchaseItems());

        PurchaseServiceResponse result = purchaseService.addPurchase(purchase);

        return new ResponseEntity<>(
                result,
                result.getResponseHttpStatusCode()
        );
    }

    //Ñapa del Object
    @GetMapping("/purchases/{id}")
    public ResponseEntity<Object> getPurchaseById(@PathVariable String id) {
        System.out.println("ProductoController - getPurchaseById()");
        System.out.println("ProductoController - la id del purchase a buscar es : "+id);

        Optional<PurchaseModel> result = purchaseService.findById(id);

        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "Purchase no encontrado",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }
}
