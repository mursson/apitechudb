package com.techu.apitechudb.controllers;


import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.services.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.DELETE})
public class UserController {

    @Autowired
    UserService userService;

    @GetMapping("/users")
    public ResponseEntity<List<UserModel>> getUsers(
            @RequestParam (name = "$orderby", required = false) String orderBy
    ) {
        System.out.println("UserController - getUsers");

        return new ResponseEntity<>(
                userService.getUsers(orderBy),
                HttpStatus.OK
        );
    }
/*
    @GetMapping("/usersorder")
    public ResponseEntity<List<UserModel>> getUsersOrderByAge() {
        System.out.println("UserController - getUsersOrderByAge");

        return new ResponseEntity<>(
                userService.findAllOrderByAge(),
                HttpStatus.OK
        );
    }
*/
    //al Loro con la
    // Ñapa del ResponseEntity<Object>
    @GetMapping("/users/{id}")
    public ResponseEntity<Object> getUserById(@PathVariable String id) {
        System.out.println("UserController - getUsersById");
        System.out.println("UserController - getUsersById -la id del usuario es: " + id);

        Optional<UserModel> respuesta= userService.findById(id);

        return new ResponseEntity<>(
                respuesta.isPresent() ? respuesta.get() : "Usuario no encontrado",
                respuesta.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    //No se pasa id para crear el objeto con el POST!!
    @PostMapping("/users")
    public ResponseEntity<UserModel> addUser(@RequestBody UserModel user) {
        System.out.println("UserController - addUser");
        //System.out.println("UserController - la id del usuario en parámetro es: " + id);
        System.out.println("UserController - el id del usuario a añadir es: " + user.getId());
        System.out.println("UserController - el name del usuario a añadir es: " + user.getName());
        System.out.println("UserController - la age del usuario a añadir es: " + user.getAge());

        return new ResponseEntity<>(
                        userService.add(user),
                        HttpStatus.CREATED
        );
    }

    @PutMapping("/users/{id}")
    public ResponseEntity<UserModel> updateUser(@RequestBody UserModel user, @PathVariable String id) {
        System.out.println("UserController - updateUser");
        System.out.println("UserController - la id del usuario en parámetro es: " + id);
        System.out.println("UserController - el id del usuario a actualizar es: " + user.getId());
        System.out.println("UserController - el name del usuario a actualizar es: " + user.getName());
        System.out.println("UserController - la age del usuario a actualizar es: " + user.getAge());

        Optional<UserModel> userToUpdate= userService.findById(id);

        if(userToUpdate.isPresent()) {
            System.out.println("UserController - dentro del if isPresent()");

            userService.update(user);
        }

        return new ResponseEntity<>(
                user,
                userToUpdate.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    @DeleteMapping("/users/{id}")
    public ResponseEntity deleteUser(@PathVariable String id) {
        System.out.println("UserController - deleteUser");
        System.out.println("UserController - deleteUser - la id del usuario a borrar es: " + id);

        boolean deleteUser = userService.delete(id);

        return new ResponseEntity<>(
                deleteUser ? "Usuario borrado" : "Usuario no borrado",
                deleteUser ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }
}
