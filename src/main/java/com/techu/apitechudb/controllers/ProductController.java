package com.techu.apitechudb.controllers;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")
@CrossOrigin(origins = "*", methods = {RequestMethod.GET, RequestMethod.DELETE})
public class ProductController {

    @Autowired
    ProductService productService;

    @GetMapping("/products")
    public ResponseEntity<List<ProductModel>> getProducts() {
        System.out.println("ProductoController - getProducts()");

        //Añadimos lógica de negocio al controlador mediante ResponseEntity
        //return productService.findAll();
        return new ResponseEntity<>(
                productService.findAll(),
                HttpStatus.OK
        );
    }

    //al Loro con la
    // Ñapa del ResponseEntity<Object>
    @GetMapping("/products/{id}")
    public ResponseEntity<Object> getProductById(@PathVariable String id) {
        System.out.println("ProductoController - getProductById()");
        System.out.println("ProductoController - la id del elemento a buscar es : "+id);

        Optional<ProductModel> result = productService.findById(id);

        return new ResponseEntity<>(
                result.isPresent() ? result.get() : "Producto no encontrado",
                result.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    @PostMapping("/products")
    public ResponseEntity<ProductModel> addProduct(@RequestBody ProductModel product) {
        System.out.println("ProductoController - addProduct()");
        System.out.println("ProductoController - la id del producto a crear es :" + product.getId());
        System.out.println("ProductoController - la desc del producto a crear es :" + product.getDesc());
        System.out.println("ProductoController - el price del producto a crear es :" + product.getPrice());

        return new ResponseEntity<>(
                //new ProductModel(),
                //productService.add(new ProductModel(product.getId(), product.getDesc(), product.getPrice())),
                productService.add(product),
                HttpStatus.CREATED
        );
    }

    @PutMapping("/products/{id}")
    public ResponseEntity updateProduct(
            @RequestBody ProductModel product, @PathVariable String id
    ) {
        System.out.println("ProductoController - updateProduct()");
        System.out.println("ProductoController - la id del producto a actualizar en parámetro URL es :" + id);
        System.out.println("ProductoController - la id del producto a actualizar es :" + product.getId());
        System.out.println("ProductoController - la desc del producto a actualizar es :" + product.getDesc());
        System.out.println("ProductoController - el price del producto a actualizar es :" + product.getPrice());

        Optional<ProductModel> productToUpdate= productService.findById(id);

        if(productToUpdate.isPresent()) {
            System.out.println("ProductoController - dentro del if");

            productService.update(product);
        }

        return new ResponseEntity<>(
                product,
                productToUpdate.isPresent() ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }

    @DeleteMapping("/products/{id}")
    public ResponseEntity deleteProduct(@PathVariable String id) {
        System.out.println("ProductoController - deleteProduct()");
        System.out.println("ProductoController - la id del producto a borrar en parámetro URL es :" + id);

        boolean deleteProduct = productService.delete(id);

        return new ResponseEntity<>(
                deleteProduct ? "Producto borrado" : "Producto no borrado",
                deleteProduct ? HttpStatus.OK : HttpStatus.NOT_FOUND
        );
    }
}
