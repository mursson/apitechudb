package com.techu.apitechudb.services;

import com.techu.apitechudb.models.PurchaseModel;
import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.repositories.PurchaseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class PurchaseService {

    @Autowired
    PurchaseRepository purchaseRepository;

    @Autowired
    ProductService productService;

    @Autowired
    UserService userService;

    public List<PurchaseModel> getPurchases() {
        System.out.println("PurchaseService - findAll()");

        return purchaseRepository.findAll();
    }

    public PurchaseServiceResponse addPurchase(PurchaseModel purchase) {
        System.out.println("PurchaseService - add()");
        System.out.println("PurchaseService - id en purchase: "+purchase.getId());
        System.out.println("PurchaseService - userId en purchase: "+purchase.getUserId());
        System.out.println("PurchaseService - amount en purchase: "+purchase.getAmount());
        System.out.println("PurchaseService - numero productos en purchase: "+purchase.getPurchaseItems().size());

        PurchaseServiceResponse result = new PurchaseServiceResponse();

        result.setPurchase(purchase);

        if(this.userService.findById(purchase.getUserId()).isEmpty()) {
            System.out.println("El usuario de la compra no se ha encontrado");

            result.setMsg("El usuario de la compra no se ha encontrado");
            result.setResponseHttpStatusCode(HttpStatus.BAD_REQUEST);

            return result;
        }

        if(this.findById(purchase.getId()).isPresent()) {
            System.out.println("Ya hay una compra con purchaseId: "+purchase.getId());

            result.setMsg("Ya hay una compra con id");
            result.setResponseHttpStatusCode(HttpStatus.BAD_REQUEST);

            return result;
        }

        float amount = 0;

        for(Map.Entry<String, Integer> purchaseItem : purchase.getPurchaseItems().entrySet()) {

            if(this.productService.findById(purchaseItem.getKey()).isEmpty()) {
                System.out.println("El producto con la id "+ purchaseItem.getKey() + "no se encuentra.");

                result.setMsg("El producto con la id "+ purchaseItem.getKey() + "no se encuentra.");
                result.setResponseHttpStatusCode(HttpStatus.BAD_REQUEST);

                return result;

            }else{
                System.out.println("Añadimos el valor de "+ purchaseItem.getValue() + " unidades del producto al total");

                amount+= (this.productService.findById( purchaseItem.getKey()).get().getPrice() * purchaseItem.getValue());

                System.out.println("Amount: "+ amount);
            }
        }

        purchase.setAmount(amount);
        this.purchaseRepository.save(purchase);
        result.setMsg("Compra añadida correctamente");
        result.setResponseHttpStatusCode(HttpStatus.CREATED);

        return result;
    }

    public Optional<PurchaseModel> findById(String id) {
        System.out.println("PurchaseService - findById()");

        return purchaseRepository.findById(id);
    }
}
