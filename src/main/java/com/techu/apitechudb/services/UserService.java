package com.techu.apitechudb.services;

import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.repositories.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

    public List<UserModel> findAll() {
        System.out.println("UserService - findAll");

        return userRepository.findAll();
    }

    //No podemos usar el valor de orderBy directamente
    //Inyección SQL
    public List<UserModel> getUsers(String orderBy) {

        List<UserModel> result;
        System.out.println("UserController - getUsers - orderBy: "+orderBy);

        if (orderBy != null) {
            System.out.println("UserController - getUsers - Se ha pedido Ordenación");

            //No se puede parametrizar!!
            result = userRepository.findAll(Sort.by("age"));

        } else {
            result= userRepository.findAll();
        }

        return result;
    }

    //Acordarse del Optional en el findById
    public Optional<UserModel> findById(String id) {
        System.out.println("UserService - findById");

        return userRepository.findById(id);
    }

    public UserModel add(UserModel user) {
        System.out.println("UserService - add");

        return userRepository.save(user);
    }

    public UserModel update(UserModel user) {
        System.out.println("UserService - update");

        return userRepository.save(user);
    }

    public boolean delete(String id) {
        System.out.println("UserService - delete");
        System.out.println("UserService - id usuario a borrar: " + id);
        boolean result= false;

        if (this.findById(id).isPresent()) {
            System.out.println("UserService - usuario " + id + " encontrado, lo borramos");

            userRepository.deleteById(id);
            result= true;
        }

        return result;
    }
}
