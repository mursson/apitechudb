package com.techu.apitechudb.services;

import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.repositories.ProductRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

//Nos lo creemos, esto cablea con el repo y el Autowired -> Inyectamos dependencias
@Service
public class ProductService {

    @Autowired
    ProductRepository productRepository;

    public List<ProductModel> findAll() {
        System.out.println("ProductService - findAll()");

        return productRepository.findAll();
    }

    public Optional<ProductModel> findById(String id) {
        System.out.println("ProductService - findById()");

        return productRepository.findById(id);
    }

    public ProductModel add (ProductModel product) {
        System.out.println("ProductService - save()");

        return productRepository.save(product);
    }

    public ProductModel update (ProductModel product) {
        System.out.println("ProductService - update()");

        return productRepository.save(product);
    }

    public boolean delete (String id) {
        System.out.println("ProductService - delete()");
        System.out.println("ProductService - id producto a borrar: " + id);
        boolean result= false;

        if (this.findById(id).isPresent()) {
            System.out.println("ProductService - producto " + id + " encontrado, lo borramos");

            productRepository.deleteById(id);
            result= true;
        }

        return result;
    }
}
