package com.techu.apitechudb.repositories;

import com.techu.apitechudb.models.UserModel;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface UserRepository extends MongoRepository<UserModel, String> {

    //Funcionalidad extra
    List<UserModel> findByName(String name);
    List<UserModel> findByAge(String age);

}
